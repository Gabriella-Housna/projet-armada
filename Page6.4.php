<!DOCTYPE html>
<html lang ="en">
    <head>
            <meta charset="utf-8">
            <link type="text/css" rel="stylesheet" href="Page6.1.css"/>
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <title>
                Armada- Ajout d'une caractéristique d'un bateau
            </title>
            <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    </head>
    <body>
            <header>
                    <div class="container-fluid" style=" padding-left: 0px; padding-right: 0px;" > 
                        <img src="Image1.png" class="img-fluid" alt="Responsive image">
                    </div>
             </header>
             <div class="container-fluid" style="background-color: navy">
                    <a href="#" class="btn" role="button" style="color:White; text-align: center"> Déconnexion </a>
                    <a href="#" class="btn btn-danger" role="button" style="color:black; text-align: center"> Acceuil </a>
                    <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>OUPS!</strong> La caractéristique que vous avez choisie a déjà été déclarée.
                          </div>
                    <h4 class="titre"><strong>Ajout d'une nouvelle caractéristique</strong></h4>
                    
                            <div class="form-group">
                                 <label for="usr" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Idnetifiant du bateau : </strong></label>
                                <input type="text" class="form-control" id="usr">
                            </div>
                            <div class="form-group">
                                <label for="sel1" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"> <strong> Veuillez sélectionner une information que vous voulez ajouter à votre bateau :</strong></label>
                                <select class="form-control" id="sel1">
                                  <option>Equipage</option>
                                  <option>Année de lancement</option>
                                  <option>Voilure</option>
                                  <option>Déplacement</option>
                                  <option>Armateur</option>
                                  <option>Port d'attache</option>
                                  <option>Maître-bau</option>
                                  <option>Longueur hors-tout</option>
                                </select>
                            </div>
                            <div class="form-group">
                                    <label for="usr" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Information : </strong></label>
                                    <input type="text" class="form-control" id="usr">
                            </div> 
                                  <input type="button" class="btn btn-dark btn-lg btn-block" value="Valider">
            </div>
            <footer>
                    <p class="end1">Copyright Armada - Tous droits réservés<br /></p>
            </footer>
    </body>

</html>