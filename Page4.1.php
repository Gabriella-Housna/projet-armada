<!DOCTYPE html>
<html lang ="en">
    <head>
            <meta charset="utf-8">
            <link type="text/css" rel="stylesheet" href="Page2.1.css"/>
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <title>
                Armada- Menu responsable bateau
            </title>
            <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    </head>
    <body>
            <header>
                    <div class="container-fluid" style=" padding-left: 0px; padding-right: 0px;" > 
                        <img src="Image1.png" class="img-fluid" alt="Responsive image">
                    </div>
             </header>
            <div class="container-fluid" style="background-color: navy">
                
                    <a href="#" class="btn" role="button" style="color:White; text-align: center"> Déconnexion </a>
                    <a href="#" class="btn btn-danger" role="button" style="color:black; text-align: center"> Acceuil </a>
                    <h4 class="titre"><strong>MENU : RESPONSABLE BATEAU</strong></h4>
                    <div class="row">
                    <div class="col-md-6">
                        <a href="#" class="btn btn-dark btn-block btn-lg" role="button" > Editer un bateau </a>
                        <a href="#" class="btn btn-dark btn-block btn-lg" role="button" > Ajouter un bateau</a>
                        <a href="#" class="btn btn-dark btn-block btn-lg" role="button" >Caractéristiques du bateau</a>
                    </div>
                    <div class="col-md-6" id="l1">
                        <h5><span class="badge badge-warning">La boutique de l'Armada est enfin en ligne</span></h5>
                    
                        <a class ="lien" href="https://www.boutiquearmada2019.fr/" id="picture">
                                 <img src="boutique.JPG" class="img-fluid" alt="Responsive image"/>
                        </a>
                        <h4><span class="badge badge-warning">Cliquez sur l'image pour y accéder </span></h4>
                    </div>
                     
                     </div>
                    
                    
                    
            </div>
            <footer>
                    <p class="end1">Copyright Armada - Tous droits réservés<br /></p>
            </footer>
    </body>
</html>