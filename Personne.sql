-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 20 Novembre 2018 à 13:05
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE `personne` (
  `Id_Perso` int(11) NOT NULL,
  `Nom_Perso` varchar(50) NOT NULL,
  `Prenom_Perso` varchar(50) NOT NULL,
  `Email_Perso` varchar(50) NOT NULL,
  `Tel_Perso` int(11) NOT NULL,
  `Mdp_Perso` varchar(50) NOT NULL,
  `Role_Perso` int(11) NOT NULL COMMENT '1 - respo bateau 2-admin 3-visiteur',
  `Demande_Perso` int(11) NOT NULL COMMENT 'Pour savoir si la personne a demandé d''avoir des droits d''accès ou pas: 0-non, 3-admin, 2-respo b'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `personne`
--

INSERT INTO `personne` (`Id_Perso`, `Nom_Perso`, `Prenom_Perso`, `Email_Perso`, `Tel_Perso`, `Mdp_Perso`, `Role_Perso`, `Demande_Perso`) VALUES
(15, 'Housna', 'Housna', 'Housna@gmail.com', 123698544, '202cb962ac59075b964b07152d234b70', 1, 0),
(24, 'Azeddine', 'azeddine', 'Azeddine@gmail.com', 1236598, '8d5e957f297893487bd98fa830fa6413', 3, 2),
(25, 'fz', 'fz', 'fz@hotmail.com', 147852, '140f6969d5213fd0ece03148e62e461e', 2, 3),
(26, 'Gabriella', 'Gabriella', 'g@gmail.com', 147852, '68053af2923e00204c3ca7c6a3150cf7', 3, 2),
(27, 'malika', 'malika', 'malika@gmail.com', 14862366, 'f355304410319e54cdcfdc86c47a0ee3', 1, 2),
(28, 'Jack', 'Jacks', 'j@gmail.com', 259843223, '4ff9fc6e4e5d5f590c4f2134a8cc96d1', 3, 3);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`Id_Perso`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `personne`
--
ALTER TABLE `personne`
  MODIFY `Id_Perso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
