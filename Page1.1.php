<!DOCTYPE html>
<html>
    <head>
         <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta charset="utf-8" />
        <link rel="stylesheet" href="Page1.css" />
        <title>Page d'accueil</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    </head>

    <body>
        <header>
            <div class="container-fluid" style=" padding-left: 0px; padding-right: 0px;" > 
                <img src="Image1.png" class="img-fluid" alt="Responsive image">
            </div>
        </header>

        <div class="container-fluid">
        <div class="row" style="background: rgb(5, 5, 179)">
                <div class="col-md-4" >
                        <h2 class="intro">Présentation de l'ARMADA : </h2>
                        <p class="intro">L'ARMADA est un large rassemblement de grands voiliers, organisé à Rouen, dans la Seine-Maritime.
                            Il est un des évènements importants du monde de la mer.
                            Il a lieu tous les 4 à 6 ans sur les quais de la seine, au sein même de la métropole Normandie.
                            Il aura lieu du 6 au 16 Juin 2019.</p>
                </div>
                <div class="col-md-4">
                        <h2 class="ind">Contact : </h2>
                        <p class="ind">Association "Armada de la liberté" <br/> Hangar 23-23, Boulevard Emile Duchemin, 76000 ROUEN <br/> Tél : 02.35.89.20.03</p>        
                </div>
                <div class="col-md-4" id="btns">
                        <button type="button" class="btn btn-outline-primary" style="box-shadow: -4px -3px black;">S'inscrire</button>  
                        <button type="button" class="btn btn-outline-primary" style="box-shadow: -4px -3px black;">Se Connecter</button>
                        <button type="button" class="btn btn-outline-primary" style="box-shadow: -4px -3px black;">Informations détaillées</button>      
                </div>
            </div>
        </div>

        <footer>
            <p class="end1">Copyright Armada - Tous droits réservés<br /></p>
        </footer>
        
    </body>

</html>