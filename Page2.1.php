<!DOCTYPE html>
<html lang ="en">
    <head>
            <meta charset="utf-8">
            <link type="text/css" rel="stylesheet" href="Page2.1.css"/>
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <title>
                Armada- Informations générales
            </title>
            <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    </head>
    <body>
            <header>
                    <div class="container-fluid" style=" padding-left: 0px; padding-right: 0px;" > 
                        <img src="Image1.png" class="img-fluid" alt="Responsive image">
                    </div>
                </header>
            <div class="container-fluid" style="background-color: navy">
                <a href="#" class="btn" role="button" style="color:White; text-align: center"> Connexion </a>
                <a href="#" class="btn btn-danger" role="button" style="color:black; text-align: center"> Acceuil </a>
                <h4 class="titre"><strong>Les navires invités</strong></h3>
                <table class="table table-hover">
                        <thead>
                          <tr>
                            <th class="titretableau">Nom</th>
                            <th class="titretableau">Pays d'origine</th>
                            <th class="titretableau">Image</th>
                            <th class="titretableau">Histoire</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="navire" data-label="Nom">Atlantis</td>
                            <td class="navire" data-label="Pays d'origine">Pays-Bas</td>
                            <td data-label="Image" class="navire"><img src="http://www.armada.org/template/img/bateau/8fc58b38200cffe45da439c4acb8e9cad1395b71.jpg" data-toggle="tooltip" data-placement="right" title="Atlantis"/></td>
                            <td class="navire" data-label="Histoire">Lancé en 1905 à Hambourg, il s’appelait à l’origine Elbe 2. Bateau sans mât à l'origine,  il débute une longue carrière de caboteur et  guide les navires dans l’estuaire de l’Elbe</td>
                          </tr>
                          <tr>
                            <td class="navire" data-label="Nom">El Galeon</td>
                            <td class="navire" data-label="Pays d'origine">Espagne</td>
                            <td data-label="Image" class="navire"><img src="http://www.armada.org/template/img/bateau/2e04f6f18c069fd5ac554809ededd4084aaebd39.jpg" data-toggle="tooltip" data-placement="right" title="El Galeón "/></td>
                            <td class="navire" data-label="Histoire">Galeón Andalucía est la réplique d'un galion espagnol du XVIe siècle conçu et construit par Ignacio Fernández Vial. Il a été parrainé et construit par la Junte d'Andalousie et la Fondation Nao Victoria avec les objectifs de promouvoir le projet Guadalquivir Rio de Historia et de rester à côté du pavillon espagnol pendant l'Exposition universelle de 2010 à Shanghai et devenir ambassadeur de la Communauté Autonome d'Andalousie</td>
                          </tr>
                          <tr>
                            <td class="navire" data-label="Nom">Tenacious</td>
                            <td class="navire" data-label="Pays d'origine">Angleterre</td>
                            <td data-label="Image" class="navire"><img src="http://www.armada.org/template/img/bateau/978a335c2e9efa9fdbdf20fcdf29698897449204.jpg"data-toggle="tooltip" data-placement="right" title="Tenacious"/></td>
                            <td class="navire" data-label="Histoire">Construit au Merlin Quay de Southampton en Angleterre et lancé en 2000, le Tenacious est l'un des plus grands voiliers en bois naviguant.</td>
                          </tr>
                          
                        </tbody>
                        <div class="col-md-4"></div>
                        <div class="col-md-4"> <td><a href="#" class="btn btn-dark" role="button" style="color:gold; font-size:14px">Afficher les informations détaillées des navires </a></td></div>
                        <div class="col-md-4"></div>
                      </table>
                      
            </div>
            
            <footer>
                    <p class="end1">Copyright Armada - Tous droits réservés<br /></p>
            </footer>
    </body>
</html>