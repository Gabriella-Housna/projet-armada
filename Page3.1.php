<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link type="text/css" rel="stylesheet" href="Page3.css"/>
        <title>Armada- page de connexion </title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    </head>

    <body>
        <header>
            <div class="container-fluid" style=" padding-left: 0px; padding-right: 0px;" > 
                <img src="Image1.png" class="img-fluid" alt="Responsive image">
            </div>
        </header>
        <div class="container-fluid" style="background-color: navy">
                   
                    <a href="#" class="btn btn-danger" role="button" style="color:black; text-align: center; float:right"> Acceuil </a>
                    <h4 class="titre" style="color: aqua"><strong>Connexion :</strong></h4>
      
 
                <div class="identification">
                     
                <div class="form-group">
                                 <label for="email" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Veuillez saisir votre votre email : </strong></label>
                                <input type="email" class="form-control" id="email">
                                <label for="pass" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Veuillez saisir votre mot de passe : </strong></label>
                                <input type="password" class="form-control" id="pass">

                </div>
                    
                    <div class="checkbox"><br/>
                        <label style="color: powderblue"><input type="checkbox"><strong> Remember me</strong></label>
                    </div>

                
                <input type="button" class="btn btn-dark btn-lg btn-block" value="Connexion">
                        
                
                </div>
                
        </div>
        
        <?php 
                include 'database.php';
                if (isset($_POST['Connexion'])){
                    extract($_POST);
                
                    if(!empty($email) && !empty($pass)){
                        $q=$db->prepare("SELECT * FROM Personne WHERE Email_Perso = :email");
                        $q->execute(['Email_Perso'=> $email]);
                        $result = $q->fetch();
                
                    if($result == true){
                        if($pass == $result['Mdp_Perso']){
                            echo "le mot de passe est bon";
                        }
                        else{
                            echo "le mot de passe est incorrect";
                        }
                    }
                    else{
                        echo "cet email n'existe pas";
                    }
                    }
                    else{
                        echo "veuillez remplir tous les champs";
                    }
                }
            ?>
        

        <footer>
             
            <p class="end1">Copyright Armada - Tous droits réservés<br /></p>
        </footer>
        
    </body>

</html>