<?php 
if (isset($_POST['nom'])) { $nom = $_POST['nom']; }
if (isset($_POST['prenom'])) { $prenom = $_POST['prenom']; }
if (isset($_POST['email'])) { $email = $_POST['email']; }
if (isset($_POST['tel'])) { $tel = $_POST['tel']; }
if (isset($_POST['pass'])) { $pass = $_POST['pass']; }
if (isset($_POST['repeatpass'])) { $repeatpass = $_POST['repeatpass']; }
$role='3';
$demande_vis='0';
$demande_resp='2';
$demande_admin='3'; 
define('HOST', 'localhost');
define('DB_NAME', 'bdd');
define('USER', 'root');
define('PASS', 'root');

try
{
	$bdd= new PDO("mysql:host=". HOST . ";dbname=". DB_NAME, USER, PASS);
}
catch(Exception $e)
{
        die('Erreur : '.$e->getMessage());
}
if(isset($_POST['inscription'])){
    $test = $bdd->query("SELECT * FROM personne WHERE Email_Perso='$email'");
    $rows = $test->rowCount();
    if($rows==0){

        if($pass==$repeatpass){
            $pass=md5($pass);
            if (isset($_POST['sel1'])) { $mySelected = $_POST['sel1']; }
            if($mySelected=='Visiteur'){
                    $req = $bdd->prepare('INSERT INTO personne VALUES(NULL, :nom, :prenom, :email, :tel, :pass, :roles, :demande)');
                    $req->execute(array(
 
	                'nom' => $nom,
	                'prenom' => $prenom,
	                'email' => $email,
	                'tel' => $tel,
                    'pass' => $pass,
                    'roles' => $role,
	                'demande' => $demande_vis,
	
    ));
    echo"<p style='color:red; font-size:20px;'>"."Inscription terminée"."</p>";
    }
            elseif($mySelected=='Responsable bateau'){
                    $req = $bdd->prepare('INSERT INTO personne VALUES(NULL, :nom, :prenom, :email, :tel, :pass, :roles, :demande)');
                    $req->execute(array(
 
                	'nom' => $nom,
	                'prenom' => $prenom,
	                'email' => $email,
	                'tel' => $tel,
                    'pass' => $pass,
                    'roles' => $role,
	                'demande' => $demande_resp,
	
    ));
    echo"<p style='color:red; font-size:20px;'>"."Inscription terminée"."</p>";
    }
            elseif($mySelected=='Administrateur'){
                    $req = $bdd->prepare('INSERT INTO personne VALUES(NULL, :nom, :prenom, :email, :tel, :pass, :roles, :demande)');
                    $req->execute(array(
 
	                'nom' => $nom,
	                'prenom' => $prenom,
	                'email' => $email,
	                'tel' => $tel,
                    'pass' => $pass,
                    'roles' => $role,
	                'demande' => $demande_admin,
	
    )); 
    echo"<p style='color:red; font-size:20px;'>"."Inscription terminée"."</p>";
    }
}else echo "<p style='color:red; font-size:20px;'>"."***Les mots de passe doivent être identiques"."</p>";
}else echo "<p style='color:red; font-size:20px;'>"."***Cette adresse email n'est pas disponible"."</p>";
}
?>
<!DOCTYPE html>
<html lang ="en">
    <head>
            <meta charset="utf-8">
            <link type="text/css" rel="stylesheet" href="Page6.1.css"/>
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <title>
                Armada- Inscription
            </title>
            <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    </head>
    <body>

        <header>    

            <div class="container-fluid" style=" padding-left: 0px; padding-right: 0px;" > 
                <img src="Image1.png" class="img-fluid" alt="Responsive image">
            </div>
        </header>
        <div class="container-fluid" style="background-color: navy">
                   
                    <a href="accueil.php" class="btn btn-danger" role="button" style="color:black; text-align: center; float:right"> Acceuil </a>
                    <h4 class="titre" style="color: aqua"><strong>Inscription :</strong></h4>

      
 

                <form method="POST" action="Page13.1.php">
                
                     
                <div class="form-group">
                                <label for="usr" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Nom : </strong></label>
                                <input type="text" class="form-control" name="nom" required>
                                <label for="usr" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Prénom: </strong></label>
                                <input type="text" class="form-control" name="prenom" required>
                                <label for="usr" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Téléphone: </strong></label>
                                <input type="text" class="form-control" name="tel" required>
                                <label for="email" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Email : </strong></label>
                                <input type="email" class="form-control" name="email" required >
                                <label for="pass" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Mot de passe : </strong></label>
                                <input type="password" class="form-control" name="pass" required>
                                <label for="pass" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Répéter mot de passe : </strong></label>
                                <input type="password" class="form-control" name="repeatpass"required>
                                <div class="form-group">
                                <label for="sel1"  style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"> <strong> Demande de droits d'accès :</strong> (La demande sera étudiée par nos administrateurs sauf pour visiteurs) </label>
                                <select class="form-control" name="sel1" id="sel1">
                                <option>Responsable bateau</option>  
                                  <option>Administrateur</option>
                                  <option>Visiteur</option>
                                </select>
                            </div>
                            <input type="submit" class="btn btn-dark btn-lg btn-block" value="Inscription" name="inscription">

                </form>
                    
     
                </div>
                
        </div>
        
        
        

        <footer>
             
            <p class="end1">Copyright Armada - Tous droits réservés<br /></p>
        </footer>
        
    </body>

</html>

