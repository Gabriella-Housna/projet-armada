-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 20 Novembre 2018 à 13:04
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `bateau`
--

CREATE TABLE `bateau` (
  `id` int(11) NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Pays` varchar(50) NOT NULL,
  `Histoire` text NOT NULL,
  `Equipage` varchar(50) NOT NULL,
  `A_lancement` varchar(20) NOT NULL,
  `DA` varchar(20) NOT NULL,
  `DD` varchar(20) NOT NULL,
  `Image` varchar(300) NOT NULL,
  `PDF` varchar(300) NOT NULL,
  `Respo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `bateau`
--

INSERT INTO `bateau` (`id`, `Nom`, `Pays`, `Histoire`, `Equipage`, `A_lancement`, `DA`, `DD`, `Image`, `PDF`, `Respo`) VALUES
(1, 'Gloria', 'Colombie', 'Le Gloria (ou ARC Gloria) est un trois-mÃ¢ts barque Ã  coque acier, construit en 1967. Il est le navire-Ã©cole de la marine colombienne.', '', '', '10/06/2019', '12/06/2019', 'pictures/etoile.jpg', 'documents/Diagramme de la BDD.pdf', 15),
(6, 'france', '', '', '', '', '', '', 'pictures/b2.jpg', 'documents/Courses_Hippiques_Correction.pdf', 15),
(7, 'Earl', 'France', 'L\'Earl of Pembroke est l\'une des derniÃ¨res goÃ©lettes Ã  coque bois construite en SuÃ¨de en 1948 et rÃ©novÃ©e dÃ¨s 1994 pour les besoins du cinÃ©ma.', '', '', '10/06/2019', '12/06/2019', 'pictures/b3.jpg', 'documents/Diagramme de la BDD.pdf', 27);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `bateau`
--
ALTER TABLE `bateau`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Respo` (`Respo`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `bateau`
--
ALTER TABLE `bateau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
