<!DOCTYPE html>
<html lang ="en">
    <head>
            <meta charset="utf-8">
            <link type="text/css" rel="stylesheet" href="Page11.1.css"/>
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <title>
                Armada- Modification des droits d'accès
            </title>
            <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    </head>
    <body>
            <header>
                    <div class="container-fluid" style=" padding-left: 0px; padding-right: 0px;" > 
                        <img src="Image1.png" class="img-fluid" alt="Responsive image">
                    </div>
             </header>
             <div class="container-fluid" style="background-color: navy">
                    <a href="#" class="btn" role="button" style="color:White; text-align: center"> Déconnexion </a>
                    <a href="#" class="btn btn-danger" role="button" style="color:black; text-align: center"> Acceuil </a>
                    <h4 class="titre"><strong>Modification des droits d'accès</strong></h4>
                    
                    <p class="intro"> Veuillez rentrer le nom et le prénom de la personne que vous cherchez :</p>
                    <div class="form-group">
                            <label for="usr" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Nom: </strong></label>
                           <input type="text" class="form-control" id="usr">
                    </div>
                    <div class="form-group">
                            <label for="usr" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Prénom : </strong></label>
                           <input type="text" class="form-control" id="usr">
                    </div>
                    <div class="alert alert-info alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Cette personne est enregistrée en tant qu'administrateur</strong> 
                     </div>
                    <table class="table-borderless container-fluid">
                        <thead>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </thead>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><a href="#" class="btn btn-dark btn-lg" role="button" style="color:gold" id="bt1">Vérifier </a></td>
                            <td></td>
                            <td></td>
                            
                        </tr>
                        <tr>
                            
                            <td><a href="#" class="btn btn-dark btn-lg" role="button" style="color:gold" id="bt2">Modifier en responsable bateau </a></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><a href="#" class="btn btn-dark btn-lg" role="button" style="color:gold" id="bt3">Modifier en administrateur </a></td>
                            
                        </tr>
                    </table>
                    

            </div>
            <footer>
                    <p class="end1">Copyright Armada - Tous droits réservés<br /></p>
            </footer>
    </body>
</html>