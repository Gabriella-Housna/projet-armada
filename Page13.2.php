<!DOCTYPE html>
<html lang ="en">
    <head>
            <meta charset="utf-8">
            <link type="text/css" rel="stylesheet" href="Page6.1.css"/>
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <title>
                Armada- Inscription
            </title>
            <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    </head>
    <body>

        <header>    

            <div class="container-fluid" style=" padding-left: 0px; padding-right: 0px;" > 
                <img src="Image1.png" class="img-fluid" alt="Responsive image">
            </div>
        </header>
        <div class="container-fluid" style="background-color: navy">
                   
                    <a href="#" class="btn btn-danger" role="button" style="color:black; text-align: center; float:right"> Acceuil </a>
                   
                    <h4 class="titre" style="color: aqua"><strong>Inscription :</strong></h4>
                    <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>ERROR!</strong> Cette adresse email existe déjà.
                          </div>
      
 
                <div class="identification">
                     
                <div class="form-group">
                                <label for="usr" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Nom : </strong></label>
                                <input type="text" class="form-control" id="usr">
                                <label for="usr" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Prénom: </strong></label>
                                <input type="text" class="form-control" id="usr">
                                <label for="usr" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Téléphone: </strong></label>
                                <input type="text" class="form-control" id="usr">
                                <label for="email" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Email : </strong></label>
                                <input type="email" class="form-control" id="email">
                                <label for="pass" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"><strong> Mot de passe : </strong></label>
                                <input type="password" class="form-control" id="pass">
                                <div class="form-group">
                                <label for="sel1" style="color: powderblue; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: 20px"> <strong> Demande de droits d'accès :</strong> (La demande sera étudiée par nos administrateurs sauf pour visiteurs) </label>
                                <select class="form-control" id="sel1">
                                <option>Responsable bateau</option>  
                                  <option>Administrateur</option>
                                  <option>Visiteur</option>
                                  
                                 
                                </select>
                            </div>

                </div>
                    
                    <div class="checkbox"><br/>
                        <label style="color: powderblue"><input type="checkbox"><strong> J'accepte les conditions d'utilisation</strong></label>
                    </div>

                
                <input type="button" class="btn btn-dark btn-lg btn-block" value="Inscription">
                        
                
                </div>
                
        </div>
        
        
        

        <footer>
             
            <p class="end1">Copyright Armada - Tous droits réservés<br /></p>
        </footer>
        
    </body>

</html>